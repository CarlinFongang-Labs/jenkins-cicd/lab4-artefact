# Jenkins | Génération de l'artifact

[Please find the specifications by clicking](https://github.com/eazytraining/")

------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com


><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Prerequisites
1. Create a job named "Artifact".
2. Push the built image using the docker-build-step plugin to DockerHub.
3. Utilize the concept of secrets to pass your DockerHub login and password.
4. Install Jenkins. [How to install Jenkins here](https://gitlab.com/CarlinFongang-Labs/jenkins-cicd/lab1-install.git)

## Plugin Installation
Dashboard > Jenkins Administration > Plugins > docker-build-step
 1. Docker-build-step
 >![alt text](img/image.png)
 2. http_request
 >![alt text](img/image-1.png) 
 3. Download and restart after installation
 >![alt text](img/image-2.png)
 >![alt text](img/image-3.png)
 
## Configuration of docker-build-step
Dashboard > Jenkins Administration > System Configuration
Enter `unix:///var/run/docker.sock`, launch the Test Connection to verify that Jenkins is connected to the Docker daemon. Once the test is performed, save the configuration.
>![alt text](img/image-4.png)

## Creating the "Artifact" Job
Create a new "Item" named "Artifact"
Dashboard > New Item > Name
Select "Free-style project" and proceed with the job creation step.
>![alt text](img/image-5.png)

## Configuring the "Artifact" Job
1. Check the "Github project" checkbox.
Dashboard > Artifact > Configuration > Github project
>![alt text](img/image-6.png)

2. Check the "This build is parameterized" checkbox.
   Configuration of variables:
   - Add parameter > String parameter
   - Set IMAGE_NAME to "alpinehelloworld" and IMAGE_TAG to "latest"
>![alt text](img/image-7.png)
>![alt text](img/image-8.png)

3. Build Configuration
   Dashboard > Artifact > Configuration > Build Step > Add a Build Step
   Select "Execute Shell" from the options.
````
#!/bin/bash
# build
docker build -t carlfg/${IMAGE_NAME}:${IMAGE_TAG} .
docker run  -d -p 80:5000 -e PORT=5000 --name ${IMAGE_NAME} carlfg/${IMAGE_NAME}:${IMAGE_TAG}
sleep 5
````
>![alt text](img/image-9.png)

4. HTTP Request Configuration
   Dashboard > Artifact > Configuration > Build Step > Add a Build Step
   Select "HTTP Request", fill in: `http://172.17.0.1`.
>![alt text](img/image-10.png)

5. Configuring the "200" Test
   Set the return code: Under "HTTP Request," click on the advanced button.
>![alt text](img/image-11.png)

6. Adding a Second Test "curl"
````
#!/bin/bash
curl -I http://172.17.0.1 
````
>![alt text](img/image-12.png)
>![alt text](img/image-20.png)
*Sortie console du curl*


8. Defining Confidential Information
Dashboard > Manage Jenkins > Credentials
>![alt text](img/image-14.png)
>![alt text](img/image-15.png)

System > Global Credentials (Unlimited) > + Add Credentials
The "Username" and "Password" correspond to the Dockerhub login credentials.
>![alt text](img/image-16.png)
>![alt text](img/image-17.png)
*Valider en cliquant sur OK*

>![alt text](img/image-18.png)

9. Finalization of the job: image push
Add a step: Execute Docker Command
````
carlfg/${IMAGE_NAME}
${IMAGE_TAG}
https://index.docker.io/v2
````
>![alt text](img/image-19.png)

10. Sortie console
>![alt text](img/image-21.png)

11. Dockerhub push result
>![alt text](img/image-13.png)
>![alt text](img/image-22.png)




















