#!/bin/bash
# build
docker build -t carlfg/${IMAGE_NAME}:${IMAGE_TAG} .
docker run  -d -p 80:5000 -e PORT=5000 --name ${IMAGE_NAME} carlfg/${IMAGE_NAME}:${IMAGE_TAG}
sleep 5

#test
#!/bin/bash
curl -I http://172.17.0.1 